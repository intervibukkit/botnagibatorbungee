package ru.intervi.botnagibatorbungee;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerDisconnectEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;

public class Events implements Listener {
	private Main main;
	
	private TaskScheduler timer = null;
	private ScheduledTask login = null;
	private ScheduledTask prelogin = null;
	private ScheduledTask mch = null;
	private FastCheck fs = new FastCheck();
	private Check check = new Check();
	private MainCheck mcheck = new MainCheck();
	
	protected Events(Main m) {
		main = m;
	}
	
	private int c = 0, l = 0; private boolean attack = false;
	public ArrayList<byte[]> whitelist = new ArrayList<byte[]>();
	public ArrayList<Data> maincheck = new ArrayList<Data>();
	
	private class Data { //класс с данными на обработку подключения к нужному серверу
		Data(ProxiedPlayer p, long time) {
			player = p;
			stamp = time;
			hash = p.getName().hashCode();
		}
		
		ProxiedPlayer player;
		long stamp;
		int hash;
	}
	
	private boolean isWhite(byte[] b) { //находится ли ip игрока в белом списке
		boolean result = false;
		try {
			for (int i = 0; i < whitelist.size(); i++) {
				if (Arrays.equals(whitelist.get(i), b)) {
					result = true;
					break;
				}
			}
		} catch (Exception e) {e.printStackTrace();}
		return result;
	}
	
	@EventHandler
	public void onConnect(ServerConnectEvent event) { //ивент подключения к серверу
		if (main.config.firststart) return;
		
		c++;
		if (c >= main.config.connects) { //проверка на атаку
			if (!attack) {
				System.out.println("[BotNagibatorBungee] !!! Началась атака!");
				attack = true;
			}
			check.p = 0;
		}
		
		if (!main.config.force) return;
		ProxiedPlayer player = event.getPlayer();
		if (attack) {
			if (!isWhite(player.getAddress().getAddress().getAddress())) event.setTarget(main.map.get(main.config.filter));
			else event.setTarget(main.map.get(main.config.main));
		} else {
			event.setTarget(main.map.get(main.config.main));
			Data data = new Data(player, System.currentTimeMillis());
			if (main.config.safecon) maincheck.add(data);
		}
	}
	
	@EventHandler
	public void onPreLogin(PreLoginEvent event) { //ивент подключения
		if (main.config.firststart) return;
		
		byte[] adr = event.getConnection().getAddress().getAddress().getAddress();
		
		if (main.config.dropfastcon) {
			l++;
			if (l >= main.config.maxcon) { //проверка лимита подключений
				fs.drop = true;
				if (!fs.fattack) fs.fattack = true;
				fs.p = 0;
			}
			
			if (fs.drop) { //подключение отменяется, если их слишком много
				if (main.config.chwhitelist) {
					if (!isWhite(adr)) {
						event.setCancelReason(main.config.reason);
						event.setCancelled(true);
						return;
					}
				} else {
					event.setCancelReason(main.config.reason);
					event.setCancelled(true);
					return;
				}
			}
		}
		
		if (main.config.iplimit && attack | fs.fattack) { //проверка лимита подключений с одного ip
			int ip = 0;
			try {
				Iterator<ProxiedPlayer> iter = main.getProxy().getPlayers().iterator();
				while (iter.hasNext()) {
					byte[] padr = iter.next().getAddress().getAddress().getAddress();
					if (Arrays.equals(adr, padr)) ip++;
					if (ip > main.config.ipjoin) {
						event.setCancelReason(main.config.ipreason);
						event.setCancelled(true);
						break;
					}
				}
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@EventHandler
	public void onDisconnect(ServerDisconnectEvent event) { //обработка отключения игрока для очистки списка
		if (main.config.safecon && !attack) {
			int hash = event.getPlayer().getName().hashCode();
			for (int i = 0; i < maincheck.size(); i++) {
				if (maincheck.get(i).hash == hash) {
					maincheck.remove(i);
					break;
				}
			}
		}
	}
	
	public void startTimer() { //старт таймера проверки
		if (!main.config.alwayson) {
			try {
				if (timer == null) timer = main.getProxy().getScheduler();
				login = timer.schedule(main, check, 1000, 1000, TimeUnit.MILLISECONDS);
				prelogin = timer.schedule(main, fs, 1000, 1000, TimeUnit.MILLISECONDS);
				mch = timer.schedule(main, mcheck, main.config.chtickms, main.config.chtickms, TimeUnit.MILLISECONDS);
			} catch (Exception e) {e.printStackTrace();}
		} else attack = true;
	}
	
	public void stopTimer() { //стоп таймера проверки
		try {
			if (timer != null) {
				if (login != null) timer.cancel(login);
				if (prelogin != null) timer.cancel(prelogin);
				if (mch != null) timer.cancel(mch);
			}
			timer = null;
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private class Check implements Runnable { //таймер проверки совершенных подключений
		private int i = 0;
		public int p = 0;
		
		@Override
		public void run() {
			if (i > main.config.inint) {
				i = 0;
				if (c < main.config.connects) p++; else p = 0;
				c = 0;
			} else i++;
			
			if (attack && p >= main.config.okchecks & c < main.config.connects & !fs.fattack && !main.config.alwayson) {
				attack = false;
				p = 0;
				System.out.println("[BotNagibatorBungee] Атака закончилась.");
			}
		}
	}
	
	private class FastCheck implements Runnable { //таймер проверки подключений
		public boolean drop = false, fattack = false;
		public int p = 0;
		
		@Override
		public void run() {
			if (drop) drop = false;
			if (l < main.config.maxcon) p++; else p = 0;
			if (fattack && p >= main.config.okchecks) fattack = false;
			l = 0;
		}
	}
	
	private class MainCheck implements Runnable { //класс проверки на нахождение на нужном сервере
		@Override
		public void run() {
			if (main.config.safecon && !attack) {
				long stamp = System.currentTimeMillis();
				for (int i = 0; i < maincheck.size(); i++) {
					Data data = maincheck.get(i);
					if ((stamp - data.stamp) >= main.config.checkms) {
						if (data.player.getServer() != null) {
							if (!data.player.getServer().getInfo().getName().equals(main.config.main)) {
								System.out.println("[BotNagibatorBungee] " + data.player.getName() + " не там, где нужно - попытка переброса на main");
								data.player.connect(main.map.get(main.config.main));
							}
						}
						maincheck.remove(i);
					}
				}
			} else if (attack) maincheck.clear();
		}
	}
}