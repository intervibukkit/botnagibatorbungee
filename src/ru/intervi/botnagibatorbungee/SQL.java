package ru.intervi.botnagibatorbungee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import net.md_5.bungee.api.scheduler.ScheduledTask;

public class SQL {
	private Main main;
	
	private static Connection con;
	private static Statement stmt;
	private static ResultSet rs;
	
	private TaskScheduler timer = null;
	private ScheduledTask task = null;
	
	protected SQL (Main m) {
		main = m;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {e.printStackTrace();}
		if (!main.config.firststart) connect();
	}
	
	public void connect() { //подключение БД
		String url = "jdbc:mysql://" + main.config.host + ":" + main.config.port + "/" + main.config.base;
		String query = "select count(*) from " + main.config.table;
		try {
			con = DriverManager.getConnection(url, main.config.user, main.config.pass);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			try {
				while (rs.next()) {
					int count = 0; count = rs.getInt(1);
					System.out.println("[BotNagibatorBungee] Строк в таблице: " + String.valueOf(count));
				}
			} catch (Exception e) {e.printStackTrace();}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch(SQLException e) {}
            try {stmt.close();} catch(SQLException e) {}
            try {rs.close();} catch(SQLException e) {}
		}
	}
	
	public void startTimer() { //старт таймера
		try {
			if (timer == null) timer = main.getProxy().getScheduler();
			task = timer.schedule(main, new TP(), main.config.tpintms, main.config.tpintms, TimeUnit.MILLISECONDS);
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public void stopTimer() { //стоп таймера
		try {
			if (timer != null && task != null) timer.cancel(task);;
			timer = null;
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private class TP implements Runnable {
		@Override
		public void run() {
			String url = "jdbc:mysql://" + main.config.host + ":" + main.config.port + "/" + main.config.base;
			String query = "select name from " + main.config.table;
			try {
				con = DriverManager.getConnection(url, main.config.user, main.config.pass);
				stmt = con.createStatement();
				rs = stmt.executeQuery(query);
				try {
					while (rs.next()) { //обработка накопившихся телепортаций
						String name = rs.getString(1);
						ProxiedPlayer player = main.getProxy().getPlayer(name);
						if (player != null) {
							System.out.println("переброс " + name + " на основной сервер");
							player.connect(main.map.get(main.config.main));
							if (main.config.addwhitelist) main.events.whitelist.add(player.getAddress().getAddress().getAddress());
						}
					}
				} catch (Exception e) {e.printStackTrace();}
				query = "DELETE FROM " + main.config.table;
				stmt.executeUpdate(query);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {con.close();} catch(SQLException e) {}
	            try {stmt.close();} catch(SQLException e) {}
	            try {rs.close();} catch(SQLException e) {}
			}
		}
	}
}