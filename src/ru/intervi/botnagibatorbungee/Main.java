package ru.intervi.botnagibatorbungee;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import ru.intervi.botnagibatorbungee.file.Config;

public class Main extends Plugin implements Listener {
	public Config config = new Config();
	public Map<String, ServerInfo> map;
	public Events events = new Events(this);
	private SQL sql = new SQL(this);
	private TaskScheduler timer = null;
	private ScheduledTask task = null;
	
	@Override
	public void onEnable() { //включение плагина
		this.getProxy().getPluginManager().registerListener(this, this);
		this.getProxy().getPluginManager().registerListener(this, events);
		this.getProxy().getPluginManager().registerCommand(this, reload);
		map = this.getProxy().getServers();
		events.startTimer();
		sql.startTimer();
		startTimer();
		System.out.println("[BotNagibatorBungee] ---> Created by InterVi <---");
	}
	
	@Override
	public void onDisable() { //выключение плагина
		events.stopTimer();
		events.whitelist.clear();
		sql.stopTimer();
		stopTimer();
	}
	
	private void startTimer() { //запуск таймера очистки памяти
		if (config.gc) {
			try {
				if (timer == null) timer = this.getProxy().getScheduler();
				task = timer.schedule(this, gc, config.gcint, config.gcint, TimeUnit.MINUTES);
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	private void stopTimer() { //стоп таймера
		try {
			if (timer != null) {
				if (task != null) timer.cancel(task);
			}
			timer = null;
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private class Reload extends Command { //обработка команды перезагрузки конфига
	    public Reload() {
	        super("bnb-reload"); //команда: /bnb-reload для перезагрузки конфига
	    }
	 
	    @Override
	    public void execute(CommandSender sender, String[] args) {
	        if (sender.hasPermission("botnagibatorbungee.reload")) {
	        	try {events.stopTimer();} catch (Exception e) {e.printStackTrace();}
	        	try {sql.stopTimer();} catch (Exception e) {e.printStackTrace();}
	        	try {stopTimer();} catch (Exception e) {e.printStackTrace();}
	        	try {config.load();} catch (Exception e) {e.printStackTrace();}
	        	try {sql.connect();} catch (Exception e) {e.printStackTrace();}
	        	try {if (!config.addwhitelist) events.whitelist.clear();} catch (Exception e) {e.printStackTrace();}
	        	try {if (!config.safecon) events.maincheck.clear();} catch (Exception e) {e.printStackTrace();}
	        	try {events.startTimer();} catch (Exception e) {e.printStackTrace();}
	        	try {sql.startTimer();} catch (Exception e) {e.printStackTrace();}
	        	try {startTimer();} catch (Exception e) {e.printStackTrace();}
	        	System.out.println("[BotNagibatorBungee] конфиг перезагружен");
	        }
	    }
	}
	private Reload reload = new Reload();
	
	private class Gc implements Runnable { //класс для вызова сборки мусора
		@Override
		public void run() {
			try {
				System.gc();
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	private Gc gc = new Gc();
}