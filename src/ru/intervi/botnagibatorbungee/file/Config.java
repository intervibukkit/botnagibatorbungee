package ru.intervi.botnagibatorbungee.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Config {
	public Config() {
		load();
	}
	
	/*
	 * botnagibatorbungee.reload - разрешение на выполнение команды /bnb-reload
	 */
	
	//настройка подключения MySQL
	public String host = "127.0.0.1"; //хост
	public String port = "3306"; //порт
	public String base = "BotNagibator"; //имя БД
	public String table = "tp"; //имя таблицы
	public String user = "root"; //пользователь
	public String pass = "lalka"; //пароль
	
	public String filter = "BotServer"; //сервер для проверки на бота
	public String main = "MainServer"; //охраняемый сервер
	
	public int connects = 7; //порог срабатывания (подключения)
	public int inint = 10; //за интервал (в секундах)
	public int okchecks = 3; //кол-во успешных проверок (по интервалу) для отключения защиты
	public int tpintms = 500; //интервал обработки телепортаций
	
	public boolean dropfastcon = true; //блокировать быстрые атаки
		public int maxcon = 20; //максимальное количество подключений в секунду
		public boolean chwhitelist = true; //проверять ли наличие ip в белом списке
		public String reason = "Идет атака. Попробуй зайти еще раз."; //сообщение кика
	
	public boolean gc = false; //включить ли принудительную собрку мусора
		public int gcint = 5; //интервал в минутах
	
	public boolean iplimit = true; //включить ли лимит подключений с одного ip (включается только во время атаки)
		public int ipjoin = 5; //максимальное кол-во подключений с одного ip
		public String ipreason = "Слишком много зашедших с одного ip."; //причина кика
	
	public boolean alwayson = false; //защита всегда включена
	public boolean addwhitelist = true; //после прохождения проверки не надо будет проходить ее снова
	public boolean force = false; //телепортация на фильтр во время атаки (если force_default_server для этого не настроен)
	
	public boolean safecon = true; //проверка подключения к серверу main
		public int checkms = 600; //через сколько проверять
		public int chtickms = 1000; //интервал обработки списка
	
	public boolean firststart = false;
	
	public void load() { //загрузка конфига
		String sep = File.separator;
		String patch = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
		patch = patch.substring(0, (patch.indexOf((sep + "plugins" + sep))));
		File folder = new File((patch + sep + "plugins" + sep + "BotNagibatorBungee"));
		if (!folder.isDirectory()) folder.mkdir();
		File conf = new File((patch + sep + "plugins" + sep + "BotNagibatorBungee" + sep + "config.yml"));
		if (!conf.isFile()) { //создание конфига по умолчанию
			firststart = true;
			try {
				FileOutputStream fos = new FileOutputStream(conf);
				InputStream stream = Config.class.getResourceAsStream("/config.yml");
				byte[] buff = new byte[65536];
				int n;
				while((n = stream.read(buff)) > 0){
					fos.write(buff, 0, n);
					fos.flush();
				}
				fos.close();
				buff = null;
			} catch(Exception e) {
				System.out.println("saveConfig " + "/config.yml" + " to " + conf.getAbsolutePath() + " Exception:");
				e.printStackTrace();
			}
		} else { //загрузка данных из файла
			ConfigLoader loader = new ConfigLoader();
			loader.load(conf);
			this.filter = loader.getString("filter");
			this.main = loader.getString("main");
			this.connects = loader.getInt("connects");
			this.inint = loader.getInt("inint");
			this.okchecks = loader.getInt("okchecks");
			this.alwayson = loader.getBoolean("alwayson");
			this.host = loader.getString("host");
			this.port = loader.getString("port");
			this.base = loader.getString("base");
			this.table = loader.getString("table");
			this.user = loader.getString("user");
			this.pass = loader.getString("pass");
			this.tpintms = loader.getInt("tpintms");
			this.addwhitelist = loader.getBoolean("addwhitelist");
			this.force = loader.getBoolean("force");
			this.dropfastcon = loader.getBoolean("dropfastcon");
			this.maxcon = loader.getInt("maxcon");
			this.chwhitelist = loader.getBoolean("chwhitelist");
			this.reason = loader.getString("reason");
			this.gc = loader.getBoolean("gc");
			this.gcint = loader.getInt("gcint");
			this.iplimit = loader.getBoolean("iplimit");
			this.ipjoin = loader.getInt("ipjoin");
			this.ipreason = loader.getString("ipreason");
			this.safecon = loader.getBoolean("safecon");
			this.checkms = loader.getInt("checkms");
			this.chtickms = loader.getInt("chtickms");
		}
	}
}